package universite.angers.master.info.server.bomberman.controller.actionnable;

/**
 * Enumération des actions possibles que peut réaliser les agents
 *
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public enum AgentAction {
	PUT_BOMB, ATTACK;
}
