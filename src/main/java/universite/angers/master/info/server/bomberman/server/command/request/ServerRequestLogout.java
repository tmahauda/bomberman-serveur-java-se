package universite.angers.master.info.server.bomberman.server.command.request;

import java.util.ArrayList;
import java.util.Collection;
import org.apache.log4j.Logger;
import universite.angers.master.info.server.bomberman.model.Player;
import universite.angers.master.info.server.bomberman.server.ServerBombermanGame;

/**
 * Classe qui permet d'envoyer une requete au serveur pour se déconnecter
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerRequestLogout extends ServerRequestAction {

	private static final Logger LOG = Logger.getLogger(ServerRequestLogout.class);

	private Player player;
	private Collection<Thread> threadRemovePlayers;

	public ServerRequestLogout() {
		this.threadRemovePlayers = new ArrayList<>();
	}

	@Override
	public boolean send(Player player) {
		LOG.debug("Request logout player : " + player);

		this.player = new Player();
		
		// On notifie le joueur
		this.player.getMessages().add("A bientôt " + player.getLogin());
		
		Player playerserver = ServerBombermanGame.getInstance().getPlayers().get(player.getId());
		LOG.debug("Player server  :" + playerserver);
		
		boolean isTurn = playerserver.isPlayTurn();
		LOG.debug("Is turn  :" + isTurn);
		
		// Si le joueur devais jouer dans ce cas on passe la main a un autre joueur
		// avant qu'il se déconnecte
		if (isTurn) {
			this.passTurnToOtherPlayer(playerserver);	
		}
		
		//Il n'est plus connecté
		playerserver.setConnected(false);
		
		// On notifie tout les joueur de la deconnexion du joueur x
		ServerBombermanGame.getInstance().notifyAllObserversWithoutPlayer(player, "NOTIFY_LOGOUT_PLAYER", player);

		//On ajoute une tache de supprimer le joueur après minute le temps d'envoyer le message
		//au joueur qui veut se déconnecter
		Thread threadRemovePlayer = new Thread(new TaskRemovePlayer(player));
		this.threadRemovePlayers.add(threadRemovePlayer);
		threadRemovePlayer.start();

		return true;
	}

	@Override
	public Player receive(Object arg) {
		this.player.setCommand("REQUEST_LOGOUT");
		return this.player;
	}

	private static class TaskRemovePlayer implements Runnable {

		private Player player;

		public TaskRemovePlayer(Player player) {
			this.player = player;
		}

		@Override
		public void run() {
			try {
				// On attend 1min avant de supprimer le temps que le serveur est le temps d'envoyer les derniers
				// messages au client
				Thread.sleep(6000);
				
				boolean remove = ServerBombermanGame.getInstance().removePlayer(this.player);
				LOG.debug("Player is remove : " + remove);

			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}
}