package universite.angers.master.info.server.bomberman.model.bomberman.agent.ennemy;

import universite.angers.master.info.server.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.server.bomberman.model.bomberman.ColorAgent;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.InfoAgent;

/**
 * Agent ennemi basique
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentEnnemyBasic extends AgentEnnemy {

	private static final long serialVersionUID = 1L;

	public AgentEnnemyBasic(String identifiant, String name, int x, int y, AgentMove move, char type, ColorAgent color,
			boolean isInvincible, boolean isSick) {
		super(identifiant, name, x, y, move, type, color, isInvincible, isSick);
	}

	public AgentEnnemyBasic(InfoAgent infoAgent) {
		super(infoAgent);
	}
}