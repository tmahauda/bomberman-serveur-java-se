package universite.angers.master.info.server.bomberman.model.bomberman.map;

import java.util.List;

import org.apache.commons.lang3.SerializationUtils;

import universite.angers.master.info.server.bomberman.controller.actionnable.AgentAction;
import universite.angers.master.info.server.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.server.bomberman.ia.solve.Solveable;
import universite.angers.master.info.server.bomberman.model.bomberman.BombermanGame;
import universite.angers.master.info.server.bomberman.model.bomberman.FactoryBombermanGame;

/**
 * Map bomberman utilisé pour la résolution en IA
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class MapAdapter extends Map implements Solveable {

	private static final long serialVersionUID = 1L;
	private BombermanGame bombermanGame;
	private List<Integer> actions;

	public MapAdapter(int maxturn, long time, List<Integer> actions) {
		this.bombermanGame = FactoryBombermanGame.getBombermanGameForIA(maxturn, time);
		this.actions = actions;
	}
	
	/**
	 * Donner la possibilité de modifier le singleton pour la
	 * recherche en IA
	 * @param instance
	 */
	public void setInstance() {
		Map.instance = this;
	}
	
	public static void setInstance(Map map) {
		Map.instance = map;
	}
	
	public static Map saveInstance() {
		return Map.instance;
	}
	
	@Override
	public boolean checkActions() {
		//On modifie l'instance du singleton
		this.setInstance();
		for(int action : this.getActions()) {
			//Si une action est moins réalisable alors
			if(this.checkActions(action))
				return true;
		}
		return false;
	}

	@Override
	public boolean checkActions(int action) {
		//On modifie l'instance du singleton
		this.setInstance();
		switch(action) {
			case 1:
				return this.getCurrentAgentKind().isLegalMove(AgentMove.MOVE_UP);
			case 2:
				return this.getCurrentAgentKind().isLegalMove(AgentMove.MOVE_DOWN);
			case 3:
				return this.getCurrentAgentKind().isLegalMove(AgentMove.MOVE_LEFT);
			case 4:
				return this.getCurrentAgentKind().isLegalMove(AgentMove.MOVE_RIGHT);
			case 5:
				return this.getCurrentAgentKind().isLegalAction(AgentAction.PUT_BOMB);
			default:
				return false;
		}
	}

	@Override
	public String applyActions(int action) {
		//On modifie l'instance du singleton
		this.setInstance();
		switch(action) {
			case 1:
				this.getCurrentAgentKind().doMove(AgentMove.MOVE_UP);
				this.applyRules();
				return AgentMove.MOVE_UP.name();
			case 2:
				this.getCurrentAgentKind().doMove(AgentMove.MOVE_DOWN);
				this.applyRules();
				return AgentMove.MOVE_DOWN.name();
			case 3:
				this.getCurrentAgentKind().doMove(AgentMove.MOVE_LEFT);
				this.applyRules();
				return AgentMove.MOVE_LEFT.name();
			case 4:
				this.getCurrentAgentKind().doMove(AgentMove.MOVE_RIGHT);
				this.applyRules();
				return AgentMove.MOVE_RIGHT.name();
			case 5:
				this.getCurrentAgentKind().doAction(AgentAction.PUT_BOMB);
				this.applyRules();
				return AgentAction.PUT_BOMB.name();
			default: 
				return "";
		}
	}

	@Override
	public Solveable clone() {
		//On modifie l'instance du singleton
		this.setInstance();
		return SerializationUtils.clone(this);
	}

	@Override
	public List<Integer> getActions() {
		//On modifie l'instance du singleton
		this.setInstance();		
		return this.actions;
	}
	
	public void applyRules() {
		//On modifie l'instance du singleton
		this.setInstance();
		this.bombermanGame.takeTurn(false);
	}
	
	public void initialize() {
		//On modifie l'instance du singleton
		this.setInstance();
		this.bombermanGame.initializeGame();
	}

	/**
	 * @return the bombermanGame
	 */
	public BombermanGame getBombermanGame() {
		return bombermanGame;
	}

	/**
	 * @param bombermanGame the bombermanGame to set
	 */
	public void setBombermanGame(BombermanGame bombermanGame) {
		this.bombermanGame = bombermanGame;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MapAdapter other = (MapAdapter) obj;

		return this.getFilename().equals(other.getFilename()) && 
				this.getSize_x() == other.getSize_x() &&
				this.getSize_y() == other.getSize_y() &&
				this.getStart_Agents().equals(other.getStart_Agents()) && //Affichage
				this.getAgents().equals(other.getAgents()) && //Hiérarchie
				this.getAgentsEnnemy().equals(other.getAgentsEnnemy()) && //Méchant
				this.getAgentsKind().equals(other.getAgentsKind()); //Gentil
	}
}
