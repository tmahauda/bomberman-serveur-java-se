package universite.angers.master.info.server.bomberman.server.command.notify.rule;

import org.apache.log4j.Logger;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.server.bomberman.model.Player;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;

/**
 * Classe qui permet d'informer à la vue coté client l'impact des bombes
 * - Incrément des bombes (step1, step2, ..., boom)
 * - Mur cassé
 * - Ennemis tués
 * - Nouveau bonus placés
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerNotifyAttackBomb implements Commandable<Player> {

	private static final Logger LOG = Logger.getLogger(ServerNotifyAttackBomb.class);
	
	@Override
	public boolean send(Player player) {
		return true;
	}

	@Override
	public Player receive(Object arg) {
		LOG.debug("Notify attack bomb");

		/**
		 * Objet qui permet de regrouper les infos à envoyer au client pour le joueur
		 */
		Player player = null;
		if(arg == null) {
			player = new Player();
		} else {
			player = (Player)arg;
		}
		
		/**
		 * On recupere l'etat des murs cassables
		 */
		player.setWallsBrokable(Map.getInstance().getStart_brokable_walls());
		
		/**
		 * On recupere letat des Ennemis
		 */
		player.setAgentsEnnemy(Map.getInstance().getAgentsEnnemy());
		
		/**
		 * On recupere letat des bombs
		 */
		player.setBombs(Map.getInstance().getStart_Bombs());
		
		/**
		 * On récupère le bomberman pour les points
		 */
		player.setBomberman(Map.getInstance().getCurrentAgentKind());
		
		/**
		 * On récupère les bonus à placé dans la map
		 */
		player.setItems(Map.getInstance().getStart_Items());
		
		player.setCommand("NOTIFY_ATTACK_BOMB");
		LOG.debug("Player : " + player);
		
		return player;
	}
}
