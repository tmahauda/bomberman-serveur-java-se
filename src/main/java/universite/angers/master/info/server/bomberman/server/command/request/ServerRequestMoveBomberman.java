package universite.angers.master.info.server.bomberman.server.command.request;

import org.apache.log4j.Logger;
import universite.angers.master.info.server.bomberman.controller.moveable.AgentMove;
import universite.angers.master.info.server.bomberman.controller.moveable.AgentMoveable;
import universite.angers.master.info.server.bomberman.model.Player;
import universite.angers.master.info.server.bomberman.model.bomberman.map.Map;

/**
 * Classe qui permet d'envoyer une requete au serveur pour poser une bombe
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerRequestMoveBomberman extends ServerRequestAction {

	private static final Logger LOG = Logger.getLogger(ServerRequestMoveBomberman.class);

	private Player player;

	@Override
	public boolean send(Player player) {
		LOG.debug("Request move player : " + player);

		this.player = new Player();
		
		boolean isTurn = this.isPlayerTurn(player);
		LOG.debug("Is turn  :" + isTurn);
		
		//Si ce n'est pas à lui de jouer alors message
		if (!isTurn) {
			this.player.getMessages().add("Ce n'est pas à vous de jouer ! ");
			return true;
		}

		AgentMove move = player.getMove();
		LOG.debug("Request move : " + move);

		AgentMoveable moveable = new AgentMoveable();
		boolean record = moveable.isLegalMove(Map.getInstance().getCurrentAgentKind(), move);
		LOG.debug("Request move record : " + record);

		if (record) {
			moveable.doMove(Map.getInstance().getCurrentAgentKind(), move);
			this.passTurnToOtherPlayer(player);
		}

		this.player.setBomberman(Map.getInstance().getCurrentAgentKind());
		this.player.setMove(move);
		this.player.setRecord(record);

		return true;
	}

	@Override
	public Player receive(Object arg) {
		this.player.setCommand("REQUEST_MOVE_AGENT");
		return this.player;
	}
}
