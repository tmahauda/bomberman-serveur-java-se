package universite.angers.master.info.server.bomberman.controller.itemable.bonus.bomberman;

import universite.angers.master.info.server.bomberman.controller.itemable.ItemType;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.kind.AgentKind;

/**
 * Les patins à roulettes augmentent ou diminuent la vitesse du bomberman. 
 * Elle se traduit par un déplacement de x cases autorisé à chaque tour
 *
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentItemableBonusBombermanSpeed extends AgentItemableBonusBomberman {

	private static final long serialVersionUID = 1L;

	public AgentItemableBonusBombermanSpeed() {

	}

	@Override
	public boolean isLegalBonus(AgentKind bomberman, ItemType itemType) {
		CapacityItem capacityItem = bomberman.getCapacityItem(ItemType.SPEED);
		if(capacityItem == null) return false;
		
		switch(itemType) {
			case SPEED_DOWN:
				return true;
			case SPEED_UP:
				return true;
			case SPEED_FULL_DOWN:
				return true;
			case SPEED_FULL_UP:
				return true;
			default:
				return false;
		}
	}

	@Override
	public void doBonus(AgentKind bomberman, ItemType itemType) {
		CapacityItem capacityItem = bomberman.getCapacityItem(ItemType.SPEED);
		if(capacityItem == null) return;
		
		switch(itemType) {
			case SPEED_DOWN:
				capacityItem.decrementCapacityActualItem();
				break;
			case SPEED_UP:
				capacityItem.incrementCapacityActualItem();
				break;
			case SPEED_FULL_DOWN:
				capacityItem.putCapacityActualItemToMin();
				break;
			case SPEED_FULL_UP:
				capacityItem.putCapacityActualItemToMax();
				break;
			default:
				return;
		}
	}

}
