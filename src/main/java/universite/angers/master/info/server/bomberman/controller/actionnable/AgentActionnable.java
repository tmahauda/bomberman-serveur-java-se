package universite.angers.master.info.server.bomberman.controller.actionnable;

import java.io.Serializable;

/**
 * Réalisation des actions communes à tous les agents
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class AgentActionnable implements Actionnable, Serializable {

	private static final long serialVersionUID = 1L;

	public AgentActionnable() {
		
	}
}
