package universite.angers.master.info.server.bomberman.controller.checkable;

/**
 * Enumération des règles implémentées
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public enum RuleType {
	APPLY_BONUS, ATTACK_BOMB, ATTACK_ENNEMY, FLY_BIRD, ENNEMY_MOVE_RANDOMLY, PUT_BOMB_RANDOMLY;
}
