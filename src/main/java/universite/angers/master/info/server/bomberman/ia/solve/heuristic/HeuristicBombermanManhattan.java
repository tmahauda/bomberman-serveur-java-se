package universite.angers.master.info.server.bomberman.ia.solve.heuristic;

import universite.angers.master.info.server.bomberman.model.bomberman.agent.Agent;

/**
 * Calcul de la distance de Manhattan
 *
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class HeuristicBombermanManhattan extends HeuristicBomberman {

	public HeuristicBombermanManhattan() {

	}

	@Override
	public double calculDistance(Agent a, Agent b) {
		return Math.abs(a.getX() - b.getX()) + Math.abs(a.getY() - b.getY());
	}

	@Override
	public String toString() {
		return "HeuristicBombermanManhattan [h=" + h + ", g=" + g + ", f=" + f + ", getH()=" + getH() + ", getG()="
				+ getG() + ", getF()=" + getF() + "]";
	}
}
