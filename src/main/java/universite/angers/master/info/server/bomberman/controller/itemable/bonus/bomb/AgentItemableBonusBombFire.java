package universite.angers.master.info.server.bomberman.controller.itemable.bonus.bomb;

import universite.angers.master.info.server.bomberman.controller.itemable.ItemType;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.CapacityItem;
import universite.angers.master.info.server.bomberman.model.bomberman.agent.kind.AgentKind;

/**
 * Augmente ou diminue la portée de l'explosion des bombes de x cases
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class AgentItemableBonusBombFire extends AgentItemableBonusBomb {

	private static final long serialVersionUID = 1L;

	public AgentItemableBonusBombFire() {

	}

	@Override
	public boolean isLegalBonus(AgentKind bomberman, ItemType itemType) {
		if(bomberman == null) return false;
		if(itemType == null) return false;
		
		CapacityItem capacityItem = bomberman.getCapacityItem(ItemType.FIRE);
		if(capacityItem == null) return false;
		
		switch(itemType) {
			case FIRE_DOWN:
				return true;
			case FIRE_UP:
				return true;
			case FIRE_FULL_DOWN:
				return true;
			case FIRE_FULL_UP:
				return true;
			default:
				return false;
		}
	}

	@Override
	public void doBonus(AgentKind bomberman, ItemType itemType) {
		if(bomberman == null) return;
		if(itemType == null) return;
		
		CapacityItem capacityItem = bomberman.getCapacityItem(ItemType.FIRE);
		if(capacityItem == null) return;
		
		switch(itemType) {
			case FIRE_DOWN:
				capacityItem.decrementCapacityActualItem();
				break;
			case FIRE_UP:
				capacityItem.incrementCapacityActualItem();
				break;
			case FIRE_FULL_DOWN:
				capacityItem.putCapacityActualItemToMin();
				break;
			case FIRE_FULL_UP:
				capacityItem.putCapacityActualItemToMax();
				break;
			default:
				return;
		}
	}
}
