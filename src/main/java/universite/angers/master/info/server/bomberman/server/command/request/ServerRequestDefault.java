package universite.angers.master.info.server.bomberman.server.command.request;

import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.server.bomberman.model.Player;

/**
 * Classe qui permet de renvoyer une commande inconnu
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerRequestDefault implements Commandable<Player> {

	@Override
	public boolean send(Player player) {
		return true;
	}

	@Override
	public Player receive(Object arg) {
		Player playerMessage = new Player();
		
		playerMessage.getMessages().add("Requete inconnu");
		
		playerMessage.setCommand("REQUEST_DEFAULT");
		return playerMessage;
	}
}